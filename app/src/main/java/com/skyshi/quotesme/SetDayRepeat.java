package com.skyshi.quotesme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;

/**
 * Created by skyshi on 11/04/16.
 */
public class SetDayRepeat extends AppCompatActivity{
    public RadioGroup radioGroup;
    public RadioButton radioButton,radioButtonEveryday,radioButtonChoosenDay;
    public CheckBox check_monday,check_tuesday,check_wednesday,
                    check_thursday,check_friday,check_saturday,
                    check_sunday;
    public Button btn_accept;
    public ArrayList<String>dayweek=new ArrayList<>();
    private static final int DAY_REPEAT = 900;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_day_repeat);
        radioGroup = (RadioGroup)findViewById(R.id.radio_group);
        radioButtonEveryday = (RadioButton)findViewById(R.id.radio_btn_everyday);
        radioButtonChoosenDay = (RadioButton)findViewById(R.id.radio_btn_choosenday);
        check_monday = (CheckBox)findViewById(R.id.check_monday);
        check_tuesday = (CheckBox)findViewById(R.id.check_tuesday);
        check_wednesday = (CheckBox)findViewById(R.id.check_wednesday);
        check_thursday = (CheckBox)findViewById(R.id.check_thursday);
        check_friday = (CheckBox)findViewById(R.id.check_friday);
        check_saturday = (CheckBox)findViewById(R.id.check_saturday);
        check_sunday = (CheckBox)findViewById(R.id.check_suday);
        btn_accept = (Button)findViewById(R.id.btn_accept_repeatday);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String type = "";
                Intent a = new Intent();
                int selectedID = radioGroup.getCheckedRadioButtonId();
                radioButton = (RadioButton) findViewById(selectedID);
                if (radioButton.getText().toString().equalsIgnoreCase("everyday")) {
                    type = radioButton.getText().toString();
                } else {
                    type = radioButton.getText().toString();
                    if (check_monday.isChecked()) {
                        a.putExtra("monday", check_monday.getText().toString());
                    }
                    if (check_tuesday.isChecked()) {
                        a.putExtra("tuesday", check_tuesday.getText().toString());
                    }
                    if (check_wednesday.isChecked()) {
                        a.putExtra("wednesday", check_wednesday.getText().toString());
                    }
                    if (check_thursday.isChecked()) {
                        a.putExtra("thursday", check_thursday.getText().toString());
                    }
                    if (check_friday.isChecked()) {
                        a.putExtra("friday", check_friday.getText().toString());
                    }
                    if (check_saturday.isChecked()) {
                        a.putExtra("saturday", check_saturday.getText().toString());
                    }
                    if (check_sunday.isChecked()) {
                        a.putExtra("sunday", check_sunday.getText().toString());
                    }
                }
                a.putExtra("type", type);
                //a.putStringArrayListExtra("listDay", dayweek);
                setResult(DAY_REPEAT, a);
                finish();
            }
        });
        setCheckedDay();
    }
    public void setCheckedDay(){
        radioButtonEveryday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                check_monday.setVisibility(View.VISIBLE);
                check_tuesday.setVisibility(View.VISIBLE);
                check_thursday.setVisibility(View.VISIBLE);
                check_friday.setVisibility(View.VISIBLE);
                check_wednesday.setVisibility(View.VISIBLE);
                check_sunday.setVisibility(View.VISIBLE);
                check_saturday.setVisibility(View.VISIBLE);
            }
        });
        radioButtonChoosenDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                check_monday.setVisibility(View.GONE);
                check_tuesday.setVisibility(View.GONE);
                check_thursday.setVisibility(View.GONE);
                check_friday.setVisibility(View.GONE);
                check_wednesday.setVisibility(View.GONE);
                check_sunday.setVisibility(View.GONE);
                check_saturday.setVisibility(View.GONE);
            }
        });
    }
}
