package com.skyshi.quotesme;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by skyshi on 04/04/16.
 */
public class TimePickerDialogBuilder extends DialogFragment implements android.app.TimePickerDialog.OnTimeSetListener {
    private OnCompleteListener mListener;
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try{
            this.mListener = (OnCompleteListener)activity;
        }catch (final ClassCastException e){
            throw new ClassCastException(activity.toString()+"must implement oncomplete");
        }
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        return new android.app.TimePickerDialog(getActivity(),this,hour,minute, android.text.format.DateFormat.is24HourFormat(getActivity()));
    }

    @Override
    public void onTimeSet(android.widget.TimePicker timePicker, int hourOfDay, int minuteOfDay) {
        String AM_PM;
        if(hourOfDay<12){
            AM_PM = "AM";
        }else{
            AM_PM = "PM";
        }
        this.mListener.onComplete(hourOfDay,minuteOfDay,AM_PM);
        //mainactivity.addscedule(hourOfDay, minuteOfDay, AM_PM);
        //Toast.makeText(getActivity(),hourOfDay+" : "+minuteOfDay+" : "+AM_PM,Toast.LENGTH_SHORT).show();
    }
    public static interface OnCompleteListener{
        public abstract void onComplete(int hoursday,int minutesday,String AM_PM);
    }
}
