package com.skyshi.quotesme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

/**
 * Created by skyshi on 07/04/16.
 */
public class OptionSchedule extends AppCompatActivity implements TimePickerDialogBuilder.OnCompleteListener{
    CheckBox check_status_on,check_vibrate_on;
    TextView txt_time,txt_time_indicator,txt_repeatAt,txt_ringtone,txt_vibrate;
    Switch switch_repeatStatus;
    String status,time,timeindicator,repeat,repeatStatus,ringtone,vibrate,index;
    MySQLiteHelper db;
    RelativeLayout rel_time_option,rel_repeat_option;
    ImageView img_back_option_schedule;
    private static final int ADD_NEW_REMINDER = 989;
    private static final int DAY_REPEAT = 900;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option_schedule);
        db = new MySQLiteHelper(this);
        Intent intent = getIntent();
        if(intent!=null){
            status = intent.getStringExtra("status");
            time = intent.getStringExtra("time");
            timeindicator = intent.getStringExtra("timeindicator");
            repeat = intent.getStringExtra("repeat");
            repeatStatus = intent.getStringExtra("repeatStatus");
            ringtone = intent.getStringExtra("ringtone");
            vibrate = intent.getStringExtra("vibrate");
            index = intent.getStringExtra("index");
        }
        check_status_on = (CheckBox)findViewById(R.id.check_status_on);
        if(status.equalsIgnoreCase("")||status.equalsIgnoreCase("on")) {
            check_status_on.setChecked(true);
        }else{
            check_status_on.setChecked(false);
        }
        check_status_on.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (check_status_on.isChecked()) {
                    Log.d("checked", "status : on");
                    status = "on";
                } else {
                    Log.d("checked", "status : off");
                    status = "off";
                }
            }
        });

        check_vibrate_on = (CheckBox)findViewById(R.id.check_vibrate_on);
        if(vibrate.equalsIgnoreCase("")||vibrate.equalsIgnoreCase("on")){
            check_vibrate_on.setChecked(true);
        }else{
            check_vibrate_on.setChecked(false);
        }
        check_vibrate_on.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (check_vibrate_on.isChecked()) {
                    vibrate = "on";
                } else {
                    vibrate = "off";
                }
            }
        });

        txt_time = (TextView)findViewById(R.id.txt_time_option);
        txt_time.setText(time);

        txt_time_indicator = (TextView)findViewById(R.id.txt_time_indicator_option);
        txt_time_indicator.setText(timeindicator);

        txt_repeatAt = (TextView)findViewById(R.id.txt_option_repeat);
        txt_repeatAt.setText(repeat);

        txt_ringtone = (TextView)findViewById(R.id.txt_ringtone_option);
        if(ringtone.equalsIgnoreCase("")) {
            txt_ringtone.setText("Default");
        }else{
            txt_ringtone.setText(ringtone);
        }
        txt_vibrate = (TextView)findViewById(R.id.txt_vibrate_option);

        switch_repeatStatus = (Switch)findViewById(R.id.switc_repeat_status);
        if(repeatStatus.equalsIgnoreCase("")||repeatStatus.equalsIgnoreCase("on")){
            switch_repeatStatus.setChecked(true);
        }else{
            switch_repeatStatus.setChecked(false);
        }
        switch_repeatStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(switch_repeatStatus.isChecked()){
                    repeatStatus = "on";
                }else{
                    repeatStatus = "off";
                }
            }
        });

        rel_time_option = (RelativeLayout)findViewById(R.id.rel_time_option);
        rel_time_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePickerDialogBuilder();
            }
        });

        rel_repeat_option = (RelativeLayout)findViewById(R.id.rel_repeat_option);
        rel_repeat_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OptionSchedule.this,SetDayRepeat.class);
                startActivityForResult(intent,DAY_REPEAT);
            }
        });

        img_back_option_schedule = (ImageView)findViewById(R.id.img_back_option_schedule);
        img_back_option_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backAndSave();
            }
        });
    }
    public void showTimePickerDialogBuilder(){
        DialogFragment newDialog = new TimePickerDialogBuilder();
        newDialog.show(getSupportFragmentManager(), "timepicker");
    }

    @Override
    public void onComplete(int hoursday, int minutesday, String AM_PM) {
        time = hoursday+":"+minutesday;
        timeindicator = AM_PM;

        txt_time.setText(time);
        txt_time_indicator.setText(timeindicator);
    }
    public void backAndSave(){
        Intent intent = new Intent();
        intent.putExtra("status",status);
        intent.putExtra("time",time);
        intent.putExtra("timeindicator",timeindicator);
        intent.putExtra("repeat",repeat);
        intent.putExtra("repeatStatus",repeatStatus);
        intent.putExtra("ringtone",ringtone);
        intent.putExtra("vibrate",vibrate);
        intent.putExtra("index",index);
        setResult(ADD_NEW_REMINDER,intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //db.updateScedule(new Scedule(status,time,timeindicator,repeat,repeatStatus,ringtone,vibrate,""));
        backAndSave();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DAY_REPEAT){
            Log.d("repeatoption",data.getStringExtra("type"));
            repeat = data.getStringExtra("type")+":";
            if(data.getStringExtra("monday")!=null) {
                repeat = repeat + data.getStringExtra("monday") +",";
                Log.d("repeatoption", data.getStringExtra("monday"));
            }
            if(data.getStringExtra("tuesday")!=null) {
                repeat = repeat + data.getStringExtra("tuesday") +",";
                Log.d("repeatoption", data.getStringExtra("tuesday"));
            }
            if(data.getStringExtra("wednesday")!=null) {
                repeat = repeat + data.getStringExtra("wednesday") +",";
                Log.d("repeatoption", data.getStringExtra("wednesday"));
            }
            if(data.getStringExtra("thursday")!=null) {
                repeat = repeat + data.getStringExtra("thursday") +",";
                Log.d("repeatoption", data.getStringExtra("thursday"));
            }
            if(data.getStringExtra("friday")!=null) {
                repeat = repeat + data.getStringExtra("friday") +",";
                Log.d("repeatoption", data.getStringExtra("friday"));
            }
            if(data.getStringExtra("saturday")!=null) {
                repeat = repeat + data.getStringExtra("saturday") +",";
                Log.d("repeatoption", data.getStringExtra("saturday"));
            }
            if(data.getStringExtra("sunday")!=null) {
                repeat = repeat + data.getStringExtra("sunday") +",";
                Log.d("repeatoption", data.getStringExtra("sunday"));
            }
            repeat = repeat.substring(0,repeat.length()-1);
            txt_repeatAt.setText(repeat);
        }
    }
}
