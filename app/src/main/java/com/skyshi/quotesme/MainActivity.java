package com.skyshi.quotesme;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skyshi.quotesme.adapter.QuotesAdapter;
import com.skyshi.quotesme.adapter.SceduleAdapter;
import com.skyshi.quotesme.model.Quotes;
import com.skyshi.quotesme.model.Scedule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SceduleAdapter.ItemClickListener,TimePickerDialogBuilder.OnCompleteListener{

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToogle;
    private RelativeLayout mDrawerView;
    private RelativeLayout mDrawerRight;
    private LinearLayout lin_btn_home,lin_btn_list,lin_horizontal_quote;
    private LinearLayout lin_kat_education,lin_kat_friendship,lin_kat_love,lin_kat_spiritual,lin_kat_leadership,lin_kat_career;
    private RecyclerView rv,rv_horizontal;
    public List<Scedule> sceduleList;
    public List<Quotes>quotesList;
    public ImageView img_message_null_schedule;
    SceduleAdapter sa;
    QuotesAdapter qa;
    MySQLiteHelper db;
    public List<String>dayOffWeeek = new ArrayList<>();
    private PendingIntent alarmIntent;
    private AlarmManager am;
    private TextView txt_quote_home;
    private static final int ADD_NEW_QUOTES = 898;
    private static final int ADD_NEW_REMINDER = 989;
    private static final String MYPREFERENCE = "mypreference";
    SharedPreferences sp;
    String newquotes = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        setContentView(R.layout.activity_main);
        sp = getApplicationContext().getSharedPreferences(MYPREFERENCE,MODE_PRIVATE);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerView = (RelativeLayout)findViewById(R.id.mainView);
        mDrawerRight = (RelativeLayout)findViewById(R.id.drawerRight);
        lin_btn_home = (LinearLayout)findViewById(R.id.lin_btn_home);
        lin_btn_list = (LinearLayout)findViewById(R.id.lin_btn_list);
        txt_quote_home = (TextView)findViewById(R.id.txt_quotes_home);

        lin_kat_education = (LinearLayout)findViewById(R.id.lin_kategori_education);
        lin_kat_friendship = (LinearLayout)findViewById(R.id.lin_kategori_friendship);
        lin_kat_love = (LinearLayout)findViewById(R.id.lin_kategori_love);
        lin_kat_spiritual = (LinearLayout)findViewById(R.id.lin_kategori_spiritual);
        lin_kat_leadership = (LinearLayout)findViewById(R.id.lin_kategori_leadership);
        lin_kat_career = (LinearLayout)findViewById(R.id.lin_kategori_career);

        img_message_null_schedule = (ImageView)findViewById(R.id.img_message_zero_schedule);
        lin_horizontal_quote = (LinearLayout)findViewById(R.id.lin_horizontal_recycler);
        if(extras!=null){
            newquotes = extras.getString("newquote");
            if(extras.getString("haveSearch")!=null && extras.getString("haveSearch").equalsIgnoreCase("toSearch")){

            }
        }
        /*if(newquotes!=""){
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("newquote",newquotes);
            editor.commit();
        }*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new MySQLiteHelper(this);
        if(sp.contains("newquote")){
            txt_quote_home.setText(sp.getString("newquote",""));
        }
        lin_horizontal_quote.setVisibility(View.GONE);
        setDrawer();
        lin_btn_home.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
        lin_btn_list.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.transparent));
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mDrawerLayout.isDrawerVisible(mDrawerRight)){
                        Intent a = new Intent(MainActivity.this,InsertQuote.class);
                        startActivityForResult(a,ADD_NEW_QUOTES);
                    }else {
                        showTimePickerDialogBuilder();
                    }
                }
            });
        }

        lin_btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if(mDrawerLayout.isDrawerVisible(mDrawerRight)){
                    mDrawerLayout.closeDrawer(mDrawerRight);
                    lin_btn_home.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                    lin_btn_list.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.transparent));
                    txt_quote_home.setVisibility(View.VISIBLE);
                    lin_horizontal_quote.setVisibility(View.GONE);
                //}
            }
        });
        lin_btn_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin_btn_home.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.transparent));
                lin_btn_list.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                mDrawerLayout.openDrawer(mDrawerRight);
                txt_quote_home.setVisibility(View.GONE);
                lin_horizontal_quote.setVisibility(View.VISIBLE);
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(this);
        LinearLayoutManager llmHorizontal = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);

        rv = (RecyclerView)findViewById(R.id.rv);
        rv_horizontal = (RecyclerView)findViewById(R.id.rv_horizontal);

        rv.setHasFixedSize(true);
        rv_horizontal.setHasFixedSize(true);

        rv_horizontal.setLayoutManager(llmHorizontal);
        rv.setLayoutManager(llm);

        sceduleList = db.getAllScedule();
        if(sceduleList.isEmpty()){
            rv.setVisibility(View.GONE);
        }else{
            sa = new SceduleAdapter(sceduleList,this,this);
            rv.setAdapter(sa);
            img_message_null_schedule.setVisibility(View.GONE);
        }
        if(db.getAllQuotes().isEmpty()) {
            String[] quoteArrayEducation = getResources().getStringArray(R.array.array_quote_education);
            String[] quoteArrayFriendship = getResources().getStringArray(R.array.array_quote_friendship);
            String[] quoteArrayLove = getResources().getStringArray(R.array.array_quote_love);
            String[] quoteArraySpiritual = getResources().getStringArray(R.array.array_quote_spiritual);
            String[] quoteArrayLeadership = getResources().getStringArray(R.array.array_quote_leadership);
            String[] quoteArrayCareer = getResources().getStringArray(R.array.array_quote_career);
            for (int i = 0; i < quoteArrayEducation.length; i++) {
                db.addQuotes(new Quotes("Education", quoteArrayEducation[i]));
            }
            for (int i = 0; i < quoteArrayFriendship.length; i++) {
                db.addQuotes(new Quotes("Friendship", quoteArrayFriendship[i]));
            }
            for (int i = 0; i < quoteArrayLove.length; i++) {
                db.addQuotes(new Quotes("Love", quoteArrayLove[i]));
            }
            for (int i = 0; i < quoteArraySpiritual.length; i++) {
                db.addQuotes(new Quotes("Spiritual", quoteArraySpiritual[i]));
            }
            for (int i = 0; i < quoteArrayLeadership.length; i++) {
                db.addQuotes(new Quotes("Leadership", quoteArrayLeadership[i]));
            }
            for (int i = 0; i < quoteArrayCareer.length; i++) {
                db.addQuotes(new Quotes("Career", quoteArrayCareer[i]));
            }
        }
        quotesList = db.getAllQuotes();
        qa = new QuotesAdapter(quotesList);
        rv_horizontal.setAdapter(qa);

        lin_kat_education.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quotesList.clear();
                quotesList = db.getKategori("Education");
                qa = new QuotesAdapter(quotesList);
                rv_horizontal.setAdapter(qa);
            }
        });
        lin_kat_friendship.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quotesList.clear();
                quotesList = db.getKategori("Friendship");
                qa = new QuotesAdapter(quotesList);
                rv_horizontal.setAdapter(qa);
            }
        });
        lin_kat_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quotesList.clear();
                quotesList = db.getKategori("Love");
                qa = new QuotesAdapter(quotesList);
                rv_horizontal.setAdapter(qa);
            }
        });
        lin_kat_spiritual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quotesList = db.getKategori("Spiritual");
                qa = new QuotesAdapter(quotesList);
                rv_horizontal.setAdapter(qa);
            }
        });
        lin_kat_leadership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quotesList = db.getKategori("Leadership");
                qa = new QuotesAdapter(quotesList);
                rv_horizontal.setAdapter(qa);
            }
        });
        lin_kat_career.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quotesList = db.getKategori("Career");
                qa = new QuotesAdapter(quotesList);
                rv_horizontal.setAdapter(qa);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sp.contains("newquote")){
            txt_quote_home.setText(sp.getString("newquote",""));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToogle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToogle.syncState();
    }

    public void showTimePickerDialogBuilder(){
        DialogFragment newDialog = new TimePickerDialogBuilder();
        newDialog.show(getSupportFragmentManager(), "timepicker");
    }
    public void setDrawer(){
        mDrawerToogle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.app_name,R.string.app_name){
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if(drawerView.equals(mDrawerRight)){
                    super.onDrawerSlide(mDrawerRight, slideOffset);
                    mDrawerView.setTranslationX(slideOffset * -mDrawerView.getWidth());
                    mDrawerLayout.setScrimColor(ContextCompat.getColor(MainActivity.this, R.color.transparent));
                    mDrawerLayout.bringChildToFront(mDrawerRight);
                    mDrawerLayout.requestLayout();
                }
                if(mDrawerLayout.isDrawerVisible(mDrawerRight)){
                    txt_quote_home.setVisibility(View.GONE);
                    lin_horizontal_quote.setVisibility(View.VISIBLE);
                    lin_btn_home.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.transparent));
                    lin_btn_list.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                }else{
                    txt_quote_home.setVisibility(View.VISIBLE);
                    lin_horizontal_quote.setVisibility(View.GONE);
                    lin_btn_home.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                    lin_btn_list.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.transparent));
                }
            }
        };
        mDrawerToogle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToogle);
    }

    @Override
    public void doOpenDetail(String status,String time, String timeindicator,
                             String repeat,String repeatStatus,String ringtone,
                             String vibrate,String index) {
        Intent a = new Intent(MainActivity.this,OptionSchedule.class);
        a.putExtra("status",status);
        a.putExtra("time",time);
        a.putExtra("timeindicator",timeindicator);
        a.putExtra("repeat",repeat);
        a.putExtra("repeatStatus",repeatStatus);
        a.putExtra("ringtone",ringtone);
        a.putExtra("vibrate",vibrate);
        a.putExtra("index", index);
        startActivityForResult(a, ADD_NEW_REMINDER);
        //Toast.makeText(MainActivity.this,"OPEN to eddit REMinder",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void doSwitchOn(String status,String time, String timeindicator,
                           String repeat,String repeatStatus,String ringtone,
                           String vibrate,String index) {
        db.updateScedule(new Scedule(
                status,
                time,
                timeindicator,
                repeat,
                repeatStatus,
                ringtone,
                vibrate,
                ""
        ), index);
        sceduleList.clear();
        sceduleList = db.getAllScedule();
        for (int i = 0; i < sceduleList.size(); i++) {
            if(sceduleList.get(i).getStatus().equalsIgnoreCase("ON")){
                setScheduleAlarm(this,sceduleList.get(i).getTime(),dayOffWeeek);
            }else{
                if(am!=null){
                    am.cancel(alarmIntent);
                }
            }
        }
        sa = new SceduleAdapter(sceduleList,this,this);
        rv.setAdapter(sa);
    }

    @Override
    public void onComplete(int hoursday, int minutesday, String AM_PM) {
        MySQLiteHelper db = new MySQLiteHelper(this);
        db.addScedule(new Scedule("ON", hoursday + ":" + minutesday, AM_PM, "Everyday", "ON", "Default", "ON", ""));
        sceduleList.clear();
        sceduleList = db.getAllScedule();
        for (int i = 0; i < sceduleList.size(); i++) {
            if(sceduleList.get(i).getStatus().equalsIgnoreCase("ON")){
                setScheduleAlarm(this,sceduleList.get(i).getTime(),dayOffWeeek);
            }
        }
        sa = new SceduleAdapter(sceduleList,this,this);
        rv.setAdapter(sa);
        rv.setVisibility(View.VISIBLE);
        img_message_null_schedule.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==ADD_NEW_QUOTES){
            if(resultCode == Activity.RESULT_OK) {
                String kategori = data.getStringExtra("kategori");
                String quote = data.getStringExtra("quote");
                Log.d("activityresult", "kategori : " + kategori + "quote: " + quote);
                MySQLiteHelper db = new MySQLiteHelper(this);
                db.addQuotes(new Quotes(kategori, quote));
                quotesList.clear();
                quotesList = db.getAllQuotes();
                qa = new QuotesAdapter(quotesList);
                rv_horizontal.setAdapter(qa);
            }else{
                return;
            }
        }
        if(requestCode==ADD_NEW_REMINDER){
            //if(resultCode == Activity.RESULT_OK){
                String status = data.getStringExtra("status");
                String time = data.getStringExtra("time");
                String timeindicator = data.getStringExtra("timeindicator");
                String repeat = data.getStringExtra("repeat");
                String repeatStatus = data.getStringExtra("repeatStatus");
                String ringtone = data.getStringExtra("ringtone");
                String vibrate = data.getStringExtra("vibrate");
                String index = data.getStringExtra("index");

                if(repeat.contains(":")) {
                    String[] repeats = repeat.split(":");
                    String type = repeats[0];
                    String[] days = repeats[1].split(",");
                    for (int i = 0; i <days.length ; i++) {
                        dayOffWeeek.add(days[i]);
                    }
                }
                db.updateScedule(new Scedule(
                        status,
                        time,
                        timeindicator,
                        repeat,
                        repeatStatus,
                        ringtone,
                        vibrate,
                        ""
                ),index);
                sceduleList.clear();
                sceduleList = db.getAllScedule();
                for (int i = 0; i < sceduleList.size(); i++) {
                    if(sceduleList.get(i).getStatus().equalsIgnoreCase("ON")){
                    setScheduleAlarm(this,sceduleList.get(i).getTime(),dayOffWeeek);
                    }
                }
                sa = new SceduleAdapter(sceduleList,this,this);
                rv.setAdapter(sa);
            //}else{
            //    return;
            //}
        }
    }
    public void setScheduleAlarm(Context context,String time, List<String> day){
        // time at which alarm will be scheduled here alarm is scheduled at 1 day from current time,
        // we fetch  the current time in milliseconds and added 1 day time
        // i.e. 24*60*60*1000= 86,400,000   milliseconds in a day
        //Long time = new GregorianCalendar().getTimeInMillis()+5*1000;//5 seconds

        Intent intentAlarm = new Intent(this,AlarmReceiver.class);
        am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intentAlarm, 0);

        String[] times = time.split(":");
        String hour = times[0];
        String minutes = times[1];

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
        calendar.set(Calendar.MINUTE,Integer.parseInt(minutes));

        String weekDay ="";
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if(Calendar.MONDAY == dayOfWeek){
            weekDay = "monday";
        }else if(Calendar.TUESDAY ==dayOfWeek){
            weekDay = "tuesday";
        }else if(Calendar.WEDNESDAY ==dayOfWeek){
            weekDay = "wednesday";
        }else if(Calendar.THURSDAY ==dayOfWeek){
            weekDay = "thursday";
        }else if(Calendar.FRIDAY ==dayOfWeek){
            weekDay = "friday";
        }else if(Calendar.SATURDAY ==dayOfWeek){
            weekDay = "saturday";
        }else if(Calendar.SUNDAY ==dayOfWeek){
            weekDay = "sunday";
        }
        if(day.size()==0) {
            //set-everyday
            am.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
            //set-chosen day
        }else{
            for (int i = 0; i <day.size() ; i++) {
                if(weekDay.equalsIgnoreCase(day.get(i).toString())){
                    am.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
                }
            }
            dayOffWeeek.clear();
        }
        //am.setRepeating(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),);
    }
    /*    @Override
    public void addScedule(int hourday, int minuteday, String am_pm) {
        MySQLiteHelper db = new MySQLiteHelper(this);
        db.addScedule(new Scedule("ON",hourday+ ":" +minuteday,am_pm,"Weekdays","","yes",""));
    }*/
}
