package com.skyshi.quotesme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by skyshi on 07/04/16.
 */
public class InsertQuote extends AppCompatActivity{
    private Spinner spinnerKategori;
    private EditText editTxt_insertQuotes;
    private Button btn_insert_quotes,btn_cancel_quotes;
    private ImageView img_back;
    private static final int ADD_NEW_QUOTES = 898;
    String kategori = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_quote);
        spinnerKategori = (Spinner)findViewById(R.id.spinnerKategori);
        List<String> list = new ArrayList<>();
        list.add("Education");
        list.add("Friendship");
        list.add("Love");
        list.add("Spiritual");
        list.add("Leadership");
        list.add("Career");
        ArrayAdapter<String>spinnerAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item,list);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerKategori.setAdapter(spinnerAdapter);
        spinnerKategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                kategori = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        editTxt_insertQuotes = (EditText)findViewById(R.id.editTxt_quote);
        btn_insert_quotes = (Button)findViewById(R.id.btn_insert_quote);
        btn_insert_quotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String quote = editTxt_insertQuotes.getText().toString();
                Intent intent = new Intent();
                intent.putExtra("quote",quote);
                intent.putExtra("kategori",kategori);
                setResult(ADD_NEW_QUOTES,intent);
                finish();
            }
        });
        btn_cancel_quotes = (Button)findViewById(R.id.btn_cancel_inser_quote);
        btn_cancel_quotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        img_back = (ImageView)findViewById(R.id.img_back_add_quote);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
