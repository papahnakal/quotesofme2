package com.skyshi.quotesme;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.skyshi.quotesme.model.Quotes;
import com.skyshi.quotesme.model.Scedule;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by skyshi on 06/04/16.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "DBQUOTESME";
    private static final String TABLE_SCEDULE = "SCEDULE";
    private static final String TABLE_QUOTE = "QUOTE";

    private static final String KEY_ID = "id";
    private static final String KEY_STATUS = "status";
    private static final String KEY_TIME = "time";
    private static final String KEY_TIMEINDICATOR = "timeIndicator";
    private static final String KEY_REPEATAT = "repeatAt";
    private static final String KEY_STATUSREPEAT = "statusRepeat";
    private static final String KEY_RINGTONE = "ringtone";
    private static final String KEY_VIBRATE = "vibrate";
    private static final String KEY_LABEL = "label";

    private static final String KEY_ID_QUOTE = "id";
    private static final String KEY_KATEGORI = "kategori";
    private static final String KEY_QUOTESTRING = "quotestring";

    private static final String[] COLUMNS = {KEY_ID,KEY_STATUS,KEY_TIME,
                                            KEY_REPEATAT,KEY_STATUSREPEAT,
                                            KEY_RINGTONE,KEY_VIBRATE,
                                            KEY_LABEL};
    private static final String[] COLUMNS_QUOTE ={KEY_ID_QUOTE,KEY_KATEGORI,KEY_QUOTESTRING};
    public MySQLiteHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_SCEDULE_TABLE = "CREATE TABLE "+TABLE_SCEDULE+" ( "+
                "id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "status TEXT, "+
                "time TEXT, "+
                "timeIndicator TEXT, "+
                "repeatAt TEXT, "+
                "statusRepeat TEXT, "+
                "ringtone TEXT, "+
                "vibrate TEXT, "+
                "label TEXT ) ";
        String CREATE_QUOTE_TABLE = "CREATE TABLE "+TABLE_QUOTE+" ( "+
                "id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "kategori TEXT, "+
                "quotestring TEXT )";
        sqLiteDatabase.execSQL(CREATE_SCEDULE_TABLE);
        sqLiteDatabase.execSQL(CREATE_QUOTE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SCEDULE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_QUOTE);
        this.onCreate(sqLiteDatabase);
    }

    public void addScedule(Scedule sceduler){
        Log.d("database", "addScedule : " + sceduler);
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_STATUS, sceduler.getStatus());
        cv.put(KEY_TIME, sceduler.getTime());
        cv.put(KEY_TIMEINDICATOR, sceduler.getTimeIndicator());
        cv.put(KEY_REPEATAT, sceduler.getRepeatAt());
        cv.put(KEY_STATUSREPEAT, sceduler.getStatusRepeat());
        cv.put(KEY_RINGTONE, sceduler.getRingtone());
        cv.put(KEY_VIBRATE, sceduler.getVibrate());
        cv.put(KEY_LABEL, sceduler.getLabel());
        db.insert(TABLE_SCEDULE, null, cv);
        db.close();
    }

    public Scedule getScedule(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =
                db.query(TABLE_SCEDULE, COLUMNS, " id= ?",
                        new String[]{String.valueOf(id)},
                        null, null, null, null);
        if(cursor!=null)
            cursor.moveToFirst();
        Scedule scedule = new Scedule();
        scedule.setId(Integer.parseInt(cursor.getString(0)));
        scedule.setStatus(cursor.getString(1));
        scedule.setTime(cursor.getString(2));
        scedule.setTimeIndicator(cursor.getString(3));
        scedule.setRepeatAt(cursor.getString(4));
        scedule.setStatusRepeat(cursor.getString(5));
        scedule.setRingtone(cursor.getString(6));
        scedule.setVibrate(cursor.getString(7));
        scedule.setLabel(cursor.getString(8));
        Log.d("database", "getScedule " + id + " : " + scedule);
        return scedule;
    }

    public List<Scedule> getAllScedule(){
        List<Scedule> scedules = new LinkedList<>();
        String query = "SELECT * FROM " +TABLE_SCEDULE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Scedule scedule = null;
        if(cursor.moveToFirst()){
            do{
                scedule = new Scedule();
                scedule.setId(Integer.parseInt(cursor.getString(0)));
                scedule.setStatus(cursor.getString(1));
                scedule.setTime(cursor.getString(2));
                scedule.setTimeIndicator(cursor.getString(3));
                scedule.setRepeatAt(cursor.getString(4));
                scedule.setStatusRepeat(cursor.getString(5));
                scedule.setRingtone(cursor.getString(6));
                scedule.setVibrate(cursor.getString(7));
                scedule.setLabel(cursor.getString(8));
                scedules.add(scedule);
            }while (cursor.moveToNext());
        }
        Log.d("database", "getAll : " + scedules);
        return scedules;
    }

    public Scedule updateScedule(Scedule scedule,String index){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_STATUS,scedule.getStatus());
        cv.put(KEY_TIME,scedule.getTime());
        cv.put(KEY_TIMEINDICATOR,scedule.getTimeIndicator());
        cv.put(KEY_REPEATAT,scedule.getRepeatAt());
        cv.put(KEY_STATUSREPEAT,scedule.getStatusRepeat());
        cv.put(KEY_RINGTONE,scedule.getRingtone());
        cv.put(KEY_VIBRATE, scedule.getVibrate());
        cv.put(KEY_LABEL, scedule.getLabel());

        db.update(TABLE_SCEDULE, cv, KEY_ID + " = " + index, null);
        Scedule scedules = new Scedule();
        //db.close();
        return scedules;
    }

    public void deleteScedule(Scedule scedule){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SCEDULE, KEY_ID + " = ?", new String[]{String.valueOf(scedule.getId())});
        db.close();
        Log.d("database", "delete : " + scedule);
    }

    public void addQuotes(Quotes quotes){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_KATEGORI, quotes.getKategori());
        cv.put(KEY_QUOTESTRING, quotes.getQuoteString());
        db.insert(TABLE_QUOTE, null, cv);
        db.close();
    }

    public Quotes getQuote(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_QUOTE, COLUMNS_QUOTE, " id = ?", new String[]{String.valueOf(id)},
                null, null, null, null);
        if(cursor!=null)
            cursor.moveToFirst();
        Quotes quote = new Quotes();
        quote.setId(Integer.parseInt(cursor.getString(0)));
        quote.setKategori(cursor.getString(1));
        quote.setQuoteString(cursor.getString(2));
        return quote;
    }

    public List<Quotes> getAllQuotes(){
        List<Quotes> quotes = new LinkedList<>();
        String query = "SELECT * FROM "+TABLE_QUOTE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Quotes quote = null;
        if(cursor.moveToFirst()){
            do{
                quote =new Quotes();
                quote.setId(Integer.parseInt(cursor.getString(0)));
                quote.setKategori(cursor.getString(1));
                quote.setQuoteString(cursor.getString(2));
                quotes.add(quote);
            }while(cursor.moveToNext());
        }
        return quotes;
    }
    public List<Quotes> getKategori(String kategori){
        List<Quotes> quotes = new LinkedList<>();
        String query = "SELECT * FROM "+TABLE_QUOTE+" WHERE "+KEY_KATEGORI +" LIKE '%"+kategori+"%'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Quotes quote = null;
        if(cursor.moveToFirst()){
            do{
                quote =new Quotes();
                quote.setId(Integer.parseInt(cursor.getString(0)));
                quote.setKategori(cursor.getString(1));
                quote.setQuoteString(cursor.getString(2));
                quotes.add(quote);
            }while(cursor.moveToNext());
        }
        return quotes;
    }

    public int updateQuotes(Quotes quotes){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(KEY_KATEGORI, quotes.getKategori());
        cv.put(KEY_QUOTESTRING, quotes.getQuoteString());

        int i = db.update(TABLE_QUOTE,cv,KEY_ID_QUOTE+" = ?",new String[]{String.valueOf(quotes.getId())});
        db.close();
        return i;
    }

    public void deleteQuotes(Quotes quotes){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_QUOTE,KEY_ID_QUOTE+" = ?",new String[]{String.valueOf(quotes.getId())});
        db.close();

    }
}
