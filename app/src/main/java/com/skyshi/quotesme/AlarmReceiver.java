package com.skyshi.quotesme;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.skyshi.quotesme.model.Quotes;

import java.util.List;
import java.util.Random;

/**
 * Created by skyshi on 08/04/16.
 */
public class AlarmReceiver extends BroadcastReceiver{
    List<Quotes> quotesList;
    @Override
    public void onReceive(Context context, Intent intent) {
            showNotification(context);
    }
    private void showNotification(Context context){
        MySQLiteHelper db = new MySQLiteHelper(context);
        quotesList = db.getAllQuotes();
        int max = quotesList.size()-1;
        int min = 0;
        Random rnd = new Random();
        int randomValue = rnd.nextInt((max-min)+1)+min;
        String messageQuote = quotesList.get(randomValue).getQuoteString();

        Intent toActivity = new Intent(context,NotificationLanding.class);
        toActivity.putExtra("message",messageQuote);
        PendingIntent contentIntent = PendingIntent.getActivity(context,0,
                toActivity,PendingIntent.FLAG_UPDATE_CURRENT);

        Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.iconapp)
                .setContentTitle("QuotesMe")
                .setContentText(messageQuote)
                .setSound(alarmsound)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageQuote));
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1,mBuilder.build());
    }
}
