package com.skyshi.quotesme.model;

/**
 * Created by skyshi on 06/04/16.
 */
public class Quotes {
    private int id;
    private String kategori;
    private String quoteString;
    public Quotes(){}
    public Quotes(String kategori,String quoteString){
        this.setKategori(kategori);
        this.setQuoteString(quoteString);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getQuoteString() {
        return quoteString;
    }

    public void setQuoteString(String quoteString) {
        this.quoteString = quoteString;
    }
}
