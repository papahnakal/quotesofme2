package com.skyshi.quotesme.model;

/**
 * Created by skyshi on 06/04/16.
 */
public class Scedule {
    private int id;
    private String status;
    private String time;
    private String timeIndicator;
    private String repeatAt;
    private String statusRepeat;
    private String ringtone;
    private String vibrate;
    private String label;
    public Scedule(){}
    public Scedule(String status, String time, String timeIndicator, String repeatAt,
                   String statusRepeat, String ringtone, String vibrate, String label){
        super();
        this.setStatus(status);
        this.setTime(time);
        this.setTimeIndicator(timeIndicator);
        this.setRepeatAt(repeatAt);
        this.setStatusRepeat(statusRepeat);
        this.setRingtone(ringtone);
        this.setVibrate(vibrate);
        this.setLabel(label);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRepeatAt() {
        return repeatAt;
    }

    public void setRepeatAt(String repeatAt) {
        this.repeatAt = repeatAt;
    }

    public String getRingtone() {
        return ringtone;
    }

    public void setRingtone(String ringtone) {
        this.ringtone = ringtone;
    }

    public String getVibrate() {
        return vibrate;
    }

    public void setVibrate(String vibrate) {
        this.vibrate = vibrate;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTimeIndicator() {
        return timeIndicator;
    }

    public void setTimeIndicator(String timeIndicator) {
        this.timeIndicator = timeIndicator;
    }

    public String getStatusRepeat() {
        return statusRepeat;
    }

    public void setStatusRepeat(String statusRepeat) {
        this.statusRepeat = statusRepeat;
    }
}
