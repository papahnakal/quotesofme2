package com.skyshi.quotesme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by skyshi on 11/04/16.
 */
public class NotificationLanding extends AppCompatActivity {
    TextView txtNotifLanding;
    FloatingActionButton float_share,float_search;
    String message = "";
    ImageView img_back;
    SharedPreferences sp;
    private static final String MYPREFERENCE = "mypreference";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        setContentView(R.layout.activity_notification_landing);
        sp = getApplicationContext().getSharedPreferences(MYPREFERENCE,MODE_PRIVATE);
        if(extras!=null){
            message = extras.getString("message");
        }
        txtNotifLanding = (TextView)findViewById(R.id.txt_notif_landing);
        txtNotifLanding.setText(message);

        img_back = (ImageView)findViewById(R.id.img_back_notification_landing);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(NotificationLanding.this, MainActivity.class);
                main.putExtra("newquote", message);
                startActivity(main);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("newquote", message);
                editor.commit();
            }
        });
        float_search = (FloatingActionButton)findViewById(R.id.float_search);
        float_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(NotificationLanding.this, MainActivity.class);
                main.putExtra("haveSearch", "toSearch");
                main.putExtra("newquote", message);
                startActivity(main);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("newquote", message);
                editor.commit();
            }
        });

        float_share = (FloatingActionButton)findViewById(R.id.float_share);
        float_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, message);
                intent.setType("text/plain");
                startActivity(Intent.createChooser(intent,"Share To"));
            }
        });
    }
}
