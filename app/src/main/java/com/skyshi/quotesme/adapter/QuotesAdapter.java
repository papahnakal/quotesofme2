package com.skyshi.quotesme.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skyshi.quotesme.R;
import com.skyshi.quotesme.model.Quotes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by skyshi on 07/04/16.
 */
public class QuotesAdapter extends RecyclerView.Adapter<QuotesAdapter.ViewHolder> {
    private List<Quotes> quotesList = new ArrayList<>();
    public QuotesAdapter(List<Quotes> quotesList){
        this.quotesList = quotesList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_rv_item,parent,false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txt_quote_horizontal.setText(quotesList.get(position).getQuoteString());
    }

    @Override
    public int getItemCount() {
        return quotesList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder  extends RecyclerView.ViewHolder{
        public TextView txt_quote_horizontal;
        public ViewHolder(View itemView) {
            super(itemView);
            txt_quote_horizontal = (TextView)itemView.findViewById(R.id.txt_quote_horizontal_item);
        }
    }
}
