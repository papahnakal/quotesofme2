package com.skyshi.quotesme.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.skyshi.quotesme.MainActivity;
import com.skyshi.quotesme.R;
import com.skyshi.quotesme.model.Scedule;

import java.util.List;

/**
 * Created by skyshi on 06/04/16.
 */
public class SceduleAdapter extends RecyclerView.Adapter<SceduleAdapter.ViewHolder>{
    private List<Scedule> sceduleList;
    private ItemClickListener listener;
    private MainActivity ma;
    private Activity act;
    public SceduleAdapter(List<Scedule> sceduleList,ItemClickListener listener,Activity act){
        this.sceduleList = sceduleList;
        this.listener = listener;
        this.act = act;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_content_schedule,parent,false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if(sceduleList.get(position).getTimeIndicator().equalsIgnoreCase("am")){
            holder.img_indicator_day.setImageResource(R.drawable.icon_am);
        }else{
            holder.img_indicator_day.setImageResource(R.drawable.icon_pm);
        }
        holder.txt_hour.setText(sceduleList.get(position).getTime());
        holder.txt_hour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status = sceduleList.get(position).getStatus();
                String time = sceduleList.get(position).getTime();
                String timeIndicator = sceduleList.get(position).getTimeIndicator();
                String repeat = sceduleList.get(position).getRepeatAt();
                String repeatStatus = sceduleList.get(position).getStatusRepeat();
                String ringtone = sceduleList.get(position).getRingtone();
                String vibrate = sceduleList.get(position).getVibrate();
                String index = sceduleList.get(position).getId() + "";
                listener.doOpenDetail(status, time, timeIndicator, repeat
                        , repeatStatus, ringtone, vibrate, index);
            }
        });
        holder.txt_repeatAt.setText(sceduleList.get(position).getRepeatAt());
        holder.txt_hour_indicator.setText(sceduleList.get(position).getTimeIndicator());
        if(sceduleList.get(position).getStatus().equalsIgnoreCase("on")) {
            holder.switch_status.setChecked(true);
            holder.card_view_scedule.setBackgroundColor(ContextCompat.getColor(act, R.color.transparent));
        }else{
            holder.switch_status.setChecked(false);
            holder.card_view_scedule.setBackgroundColor(ContextCompat.getColor(act, R.color.fullTransparent));
        }
        holder.switch_status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String status = "";
                String time = sceduleList.get(position).getTime();
                String timeIndicator = sceduleList.get(position).getTimeIndicator();
                String repeat = sceduleList.get(position).getRepeatAt();
                String repeatStatus = sceduleList.get(position).getStatusRepeat();
                String ringtone = sceduleList.get(position).getRingtone();
                String vibrate = sceduleList.get(position).getVibrate();
                String index = sceduleList.get(position).getId() + "";
                if(holder.switch_status.isChecked()){
                    holder.switch_status.setChecked(true);
                    status = "on";
                }else{
                    holder.switch_status.setChecked(false);
                    status = "off";
                }
                listener.doSwitchOn(status,time, timeIndicator, repeat
                        , repeatStatus, ringtone, vibrate, index);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sceduleList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView img_indicator_day;
        TextView txt_hour,txt_hour_indicator,txt_repeatAt;
        Switch switch_status;
        CardView card_view_scedule;
        public ViewHolder(View itemView) {
            super(itemView);
            img_indicator_day = (ImageView)itemView.findViewById(R.id.img_IndicatorDay);
            txt_hour = (TextView)itemView.findViewById(R.id.txt_hour);
            txt_hour_indicator = (TextView)itemView.findViewById(R.id.txt_hourindicator);
            txt_repeatAt = (TextView)itemView.findViewById(R.id.txt_repeatat);
            switch_status = (Switch)itemView.findViewById(R.id.switch1);
            card_view_scedule = (CardView)itemView.findViewById(R.id.card_view_scedule);
        }
    }
    public interface ItemClickListener{
        public void doOpenDetail(String status,String time, String timeindicator,
                                 String repeat,String repeatStatus,String ringtone,
                                 String vibrate,String index);
        public void doSwitchOn(String status,String time, String timeindicator,
                               String repeat,String repeatStatus,String ringtone,
                               String vibrate,String index);
    }
}
